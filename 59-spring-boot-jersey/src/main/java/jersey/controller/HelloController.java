package jersey.controller;

import org.springframework.stereotype.Component;

import javax.ws.rs.*;

/**
 * @author guo
 */
@Component
@Path("hello")
public class HelloController {

    @GET
    @Path("name")
    public String sayHi(@QueryParam("name") String name) {
        return "hello "+name;
    }
}
