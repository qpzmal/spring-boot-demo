package mp.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import mp.entity.User;
import mp.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService extends ServiceImpl<UserMapper, User> {

    @Autowired(required = false)
    private UserMapper userMapper;

    public Page<User> selectByPage(Page<User> page, String name){
        return page.setRecords(userMapper.selectByPage(page,name));
    }

}
