package kafka.controller;

import kafka.beans.Message;
import kafka.provider.KafkaSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    @Autowired
    private KafkaSender sender;

    @GetMapping("send")
    public String sendKafka(String message) {
        Message msg = new Message();
        msg.setId(1L);
        msg.setSendTime(new Date());
        msg.setMsg(message);
        sender.send(msg);
        return "success";
    }
}
