package many.redis.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HelloController {

    @Resource(name = "redisOneTemplate")
    private StringRedisTemplate redisOne;
    @Resource(name = "redisTwoTemplate")
    private StringRedisTemplate redisTwo;


    @GetMapping("one/{name}/{value}")
    public String getOne(@PathVariable String name, @PathVariable String value){
        redisOne.opsForValue().set(name,value);
        return redisOne.opsForValue().get(name);
    }


    @GetMapping("two/{name}/{value}")
    public String getTwo(@PathVariable String name, @PathVariable String value){
        redisTwo.opsForValue().set(name,value);
        return redisTwo.opsForValue().get(name);
    }
}
