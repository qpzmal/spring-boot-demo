package spring.rabbitmq.onetomany;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "onetomany")
public class OneToManyMessageReceiver2 {

    @RabbitHandler
    public void receiver2(String message) {
        System.out.println("Receiver2  : " + message);
    }

}
