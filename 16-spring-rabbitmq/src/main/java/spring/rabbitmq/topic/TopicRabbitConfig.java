package spring.rabbitmq.topic;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicRabbitConfig {

    public final static String QUEUE_A = "topic.a";
    public final static String QUEUE_B = "topic.b";

    @Bean
    public Queue queueMessageA() {
        return new Queue(TopicRabbitConfig.QUEUE_A);
    }

    @Bean
    public Queue queueMessageB() {
        return new Queue(TopicRabbitConfig.QUEUE_B);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("exchange");
    }

    @Bean
    Binding bindingExchangeMessageA(Queue queueMessageA, TopicExchange exchange) {
        return BindingBuilder.bind(queueMessageA).to(exchange).with(TopicRabbitConfig.QUEUE_A);
    }

    @Bean
    Binding bindingExchangeMessages(Queue queueMessageB, TopicExchange exchange) {
        return BindingBuilder.bind(queueMessageB).to(exchange).with("topic.#");
    }
}

