package spring.rabbitmq.manytomany;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "manytomany")
public class ManyToManyMessageReceiver1 {

    @RabbitHandler
    public void receiver1(String message) {
        System.out.println("Receiver1  : " + message);
    }

}
