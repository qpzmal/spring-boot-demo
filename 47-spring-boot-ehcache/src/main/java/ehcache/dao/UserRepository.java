package ehcache.dao;

import ehcache.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * mapper
 * @author blues
 */
@Repository
public interface UserRepository extends JpaRepository<User,String> {

}
