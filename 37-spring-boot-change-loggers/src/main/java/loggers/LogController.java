package loggers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("/test")
    public String testLogLevel(){
        logger.debug("logger level: debug");
        logger.info("logger level: info");
        logger.error("logger level: error");
        return "";
    }

}
