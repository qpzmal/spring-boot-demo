package loggers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChangeLoggersApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChangeLoggersApplication.class, args);
	}
}
