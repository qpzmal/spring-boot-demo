package transactional.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import transactional.entity.User;
import transactional.service.UserService;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/update/{id1}/{name1}/{id2}/{name2}")
    public Object updates(@PathVariable String id1,
                          @PathVariable String name1,
                          @PathVariable String id2,
                          @PathVariable String name2) throws Exception{
        User blues = new User(id1,name1);
        User zhangsan = new User(id2,name2);
        return userService.updates(blues,zhangsan);
    }
}
