package h2.service;

import h2.entity.User;
import h2.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    @Autowired(required = false)
    private UserMapper userMapper;

    public Integer insert(String id,String name){
        User user = new User(id,name);
        return userMapper.insert(user);
    }


    public User selectById(String id){
        return userMapper.selectById(id);
    }
}

