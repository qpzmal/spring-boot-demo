package spring.boot.stop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootStopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootStopApplication.class, args);
	}
}
