package lombok;

/**
 * @author guo
 */
@Data
public class User {
    private String id;
    private String name;
}
