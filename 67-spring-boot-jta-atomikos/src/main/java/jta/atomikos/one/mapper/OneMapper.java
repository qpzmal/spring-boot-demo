package jta.atomikos.one.mapper;

import jta.atomikos.model.one.One;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author guo
 */
public interface OneMapper {

    @Select("select id,one from one where id=#{id}")
    One selectById(@Param("id") String id);

    @Insert("insert into one(id,one) values(#{id},#{one})")
    Integer insert(One one);

}
