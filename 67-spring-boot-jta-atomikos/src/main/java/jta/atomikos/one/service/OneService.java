package jta.atomikos.one.service;

import jta.atomikos.one.mapper.OneMapper;
import jta.atomikos.model.one.One;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author guo
 */
@Service
public class OneService {

    @Autowired(required = false)
    private OneMapper oneMapper;


    public One selectById(String id){
        return oneMapper.selectById(id);
    }

}
