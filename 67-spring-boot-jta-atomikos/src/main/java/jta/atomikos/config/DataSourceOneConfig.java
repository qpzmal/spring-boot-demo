package jta.atomikos.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author guo
 */
@Configuration
@MapperScan(basePackages = "jta.atomikos.one.mapper",sqlSessionFactoryRef="oneSqlSessionFactory")
public class DataSourceOneConfig {

    /**
     * 配置数据源
     */
    @Bean(name="oneDatasource")
    @ConfigurationProperties(prefix = "spring.jta.atomikos.datasource.one")
    public DataSource oneDatasource(){
        return new AtomikosDataSourceBean();
    }

    /**
     * 创建SqlSessionFactory
     */
    @Bean(name="oneSqlSessionFactory")
    public SqlSessionFactory oneSqlSessionFactory(@Qualifier("oneDatasource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean=new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        return bean.getObject();
    }

    @Bean(name="oneSqlSessionTemplate")
    public SqlSessionTemplate oneSqlSessionTemplate(@Qualifier("oneSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}






