package spring.boot.mybatis;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author guo
 */
@RestController
public class SysUserController {

    @Autowired
    private SysUserMapper sysUserMapper;

    @PostMapping("/insert/{name}")
    public Integer insert(@PathVariable("name") String name){
        SysUser sysUser = new SysUser();
        sysUser.setName(name);
        return sysUserMapper.insert(sysUser);
    }

    @GetMapping("/findPage")
    public PageInfo<SysUser> findPage(){
        int pageNum = 2;
        int pageSize = 2;
        PageHelper.startPage(pageNum, pageSize);
        List<SysUser> sysUserList = sysUserMapper.findList();
        return new PageInfo<>(sysUserList);
    }

}
