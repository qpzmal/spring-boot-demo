package sqlite.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sqlite.entity.User;
import sqlite.service.UserService;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert/{id}/{name}")
    public Integer insert(@PathVariable String id, @PathVariable String name){
        User user = new User();
        user.setId(id);
        user.setName(name);
        return userService.insert(user);
    }

    @GetMapping("/selectById/{id}")
    public User selectById(@PathVariable String id){
        return userService.selectById(id);
    }

    @DeleteMapping("/deleteById/{id}")
    public Integer deleteById(@PathVariable String id){
        return userService.deleteById(id);
    }
}
