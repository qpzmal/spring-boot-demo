package sqlite.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sqlite.entity.User;
import sqlite.mapper.UserMapper;

@Service
public class UserService {

    @Autowired(required = false)
    private UserMapper userMapper;

    public Integer insert(User user){
        return userMapper.insert(user);
    }

    public User selectById(String id){
        return userMapper.selectById(id);
    }

    public Integer deleteById(String id) {
        return userMapper.deleteById(id);
    }
}
