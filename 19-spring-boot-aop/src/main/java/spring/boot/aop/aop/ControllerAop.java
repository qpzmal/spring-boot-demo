package spring.boot.aop.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @author blues
 */
@Aspect
@Component
public class ControllerAop {


    /**
     * 使用此方法不需要加注解，符合路径条件的类都会被切入
     */
//    @Pointcut("execution(public * spring.boot.aop.controller.*.*(..))")
//    public void aopMethods(){}

    /**
     * 使用此方法需要在被切入的方法上加@AopMethod注解
     */
    @Pointcut("@annotation(spring.boot.aop.aop.AopMethod)")
    public void aopMethods(){}


    @Before("aopMethods()")
    public void deBefore(JoinPoint joinPoint) throws Throwable {
        System.out.println("before method start ...");
    }


    /**
     * 后置最终通知,final增强，不管是抛出异常或者正常退出都会执行
     */
    @After("aopMethods()")
    public void after(JoinPoint jp){
        System.out.println("after method end .....");
    }


    /**
     * 后置异常通知
     * @param jp
     */
    @AfterThrowing("aopMethods()")
    public void throwers(JoinPoint jp){
        System.out.println("方法异常时执行.....");
    }


    @AfterReturning(returning = "ret", pointcut = "aopMethods()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容
        System.out.println("方法的返回值 : " + ret);
    }




    /**
     * 环绕通知,环绕增强，相当于MethodInterceptor
     * @param pjp
     * @return
     */
    @Around("aopMethods()")
    public Object around(ProceedingJoinPoint pjp) {
        System.out.println("方法环绕start.....");
        try {
            Object o =  pjp.proceed();
            System.out.println("方法环绕proceed，结果是 :" + o);
            return o;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

}
