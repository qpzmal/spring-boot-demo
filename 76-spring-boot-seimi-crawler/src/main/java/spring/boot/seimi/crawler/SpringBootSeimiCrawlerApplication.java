package spring.boot.seimi.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSeimiCrawlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSeimiCrawlerApplication.class, args);
	}

}
